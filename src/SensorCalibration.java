import lejos.robotics.Color;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.ColorAdapter;
import lejos.robotics.LightDetectorAdaptor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class SensorCalibration {
	
	static EV3 ev3 = (EV3) BrickFinder.getDefault();
	//static EV3ColorSensor lightColorSensor = new EV3ColorSensor(SensorPort.S4);
	//static ColorAdapter colorAdapter = new ColorAdapter(lightColorSensor);
	//static LightDetectorAdaptor lightDetectorAdaptor = new LightDetectorAdaptor((SampleProvider)lightColorSensor);
	//static EV3UltrasonicSensor ultrasonicSensor = new EV3UltrasonicSensor(SensorPort.S3);
	//static EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S1);
	//static LightDetectorAdaptor lightDetectorAdaptor = new LightDetectorAdaptor((SampleProvider)lightColorSensor);
	
	static EV3LargeRegulatedMotor leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
	static EV3LargeRegulatedMotor rightMotor = new EV3LargeRegulatedMotor(MotorPort.D);
	
	static double[][] recordedValuesUltrasonic;
	/*
	public static void calibrateUltrasonic(){
		SampleProvider sampleProvider = ultrasonicSensor.getDistanceMode();
		GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
		graphicsLCD.clear();
    	graphicsLCD.drawString("Ultrasonic Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		Button.waitForAnyPress();
		if(Button.readButtons() == Button.ID_RIGHT){
			float distance = 0;
		
				while(Button.readButtons() != Button.ID_ESCAPE){
					if(sampleProvider.sampleSize() > 0) {
				    	float [] sample = new float[sampleProvider.sampleSize()];
				    	sampleProvider.fetchSample(sample, 0);
				    	
				    	distance = sample[0];
				    	distance *= 100;
				    	graphicsLCD.clear();
				    	graphicsLCD.drawString("Ultrasonic Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
				    	//graphicsLCD.drawString("Measure " + (j+1), graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 , GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
				    	//graphicsLCD.drawString("Experiment "+ (i+1), graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 + 20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
						graphicsLCD.drawString(""+distance, graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 + 40, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
				    	Delay.msDelay(10);
					}
					
					Thread.yield();
				}
				
			
		}
		
		
		
	}
	public static void calibrateColor(){
		GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
		graphicsLCD.clear();
    	graphicsLCD.drawString("Color Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		Button.waitForAnyPress();
		if(Button.readButtons() == Button.ID_RIGHT){
		
				while(Button.readButtons() != Button.ID_ESCAPE){
					Color color = colorAdapter.getColor();
					float light = lightDetectorAdaptor.getLightValue();
					
					graphicsLCD.clear();
					graphicsLCD.drawString("R : " + color.getRed(), 10, 20 , GraphicsLCD.VCENTER|GraphicsLCD.LEFT);
					graphicsLCD.drawString("G : " + color.getGreen(), 10, 40 , GraphicsLCD.VCENTER|GraphicsLCD.LEFT);
					graphicsLCD.drawString("B : " + color.getBlue(), 10, 60 , GraphicsLCD.VCENTER|GraphicsLCD.LEFT);
					graphicsLCD.drawString("L : " + light, 10, 80 , GraphicsLCD.VCENTER|GraphicsLCD.LEFT);
					Delay.msDelay(1000);
					
					Thread.yield();
				}
				
			
		}
		
	}
	
	public static void calibrateGyro(){
		SampleProvider sampleProvider = gyroSensor.getAngleAndRateMode();
		GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
		graphicsLCD.clear();
    		graphicsLCD.drawString("Gyro Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		Button.waitForAnyPress();
		if(Button.readButtons() == Button.ID_RIGHT){
			float distance = 0;
		
				while(Button.readButtons() != Button.ID_ESCAPE){
					if(sampleProvider.sampleSize() > 0) {
				    	float [] sample = new float[sampleProvider.sampleSize()];
				    	sampleProvider.fetchSample(sample, 0);
				 	
				    	distance = sample[0];
				    	graphicsLCD.clear();
				    	graphicsLCD.drawString("Gyro Sensor", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
				    	//graphicsLCD.drawString("Measure " + (j+1), graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 , GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
				    	//graphicsLCD.drawString("Experiment "+ (i+1), graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 + 20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
						graphicsLCD.drawString(""+distance, graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2 + 40, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
				    	Delay.msDelay(100);
					}
					
					Thread.yield();
				}
				
			
		}
	}*/
	
	public static void move(){
		GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
		graphicsLCD.drawString("Move", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		Button.waitForAnyPress();
		int speed=100;
    	graphicsLCD.clear();
    	graphicsLCD.drawString("only left?", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
		Button.waitForAnyPress();
    	boolean onlyLeft=Button.readButtons() == Button.ID_ENTER;
		while(true){
			graphicsLCD.clear();
	    	graphicsLCD.drawString(speed+"", graphicsLCD.getWidth()/2, graphicsLCD.getHeight()/2-20, GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
	    	Button.waitForAnyPress();
	    	int button=Button.readButtons();
	    	if(button == Button.ID_RIGHT){
				speed+=100;
			}else if(button == Button.ID_LEFT){
				speed-=100;
			}else break;
	    	
		}
		leftMotor.setSpeed(speed);
		rightMotor.setSpeed(onlyLeft ? 0 : speed);
		leftMotor.forward();
		rightMotor.forward();
		Delay.msDelay(5000);
		leftMotor.setSpeed(0);
		rightMotor.setSpeed(0);
	}
	
	public static void main(String[] args){
		recordedValuesUltrasonic = new double[12][5];
		move();
		
	}

}
